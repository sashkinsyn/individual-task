//Преобразование текста в верхний регистр с помощью метода строк toUpperCase()
const allTopics = document.querySelectorAll('.basic-level .lectures-topic');
for (let lecturesTopic of allTopics) {
    lecturesTopic.innerHTML = lecturesTopic.textContent.toUpperCase();
}
//Сокращение строки при превышении длины в 20 элементов
const allSubTopics = document.querySelectorAll('.basic-level .lectures-subtitle');
for (let lecturesSubTopic of allSubTopics) {
    let checkElement = lecturesSubTopic.textContent;
    let trimElement = checkElement.trim(); // с помощью метода trim() убираем лишние пробелы в конце и начале строки, для корректного подсчёта длины строки.
    if (trimElement.length > 20) {
        const result = trimElement.slice(0, 20); // с помощью метода slice() берём от строки только первые 20 элементов 
        lecturesSubTopic.innerHTML = result + '...' // с помощью конкатенации добавляем в конец строки "..."
    }
}